function wdbg  (
    [ValidateLength(2,20)]
    [Parameter(Mandatory = $true, HelpMessage="Part of process property name (process name, executable name, apppool name etc.). 
    It can be passed a a regular expression")]
    $name,
    [Parameter(Mandatory = $false)]
    [switch]$windowsServiceInclude)
{

$allProcesses = Get-Process
 
$processes = GetProcessesesByName $name $allProcesses
$iisWorkerProcesses = GetIISWorkerProcessesByName $name $allProcesses
if ($windowsServiceInclude) {
    $windowsServiceProcesses = GetWindowsServicesByName $name $allProcesses
}
 
 Write-Host "PROCESSES FOUND:"
 Write-Host ""

 $processes | Sort-Object Name | Format-table Pid, Name, Description, ExecutableName, Architecture, ClrVersion

 Write-Host "IIS APPPOOLS FOUND:"
 Write-Host ""

 $iisWorkerProcesses | Sort-Object AppPoolName | Format-table Pid, AppPoolName, AppPoolState, Name, Description, ExecutableName, ClrVersion

 Write-Host "WINDOWS SERVICES PROCESSES FOUND:"
 Write-Host ""

 $windowsServiceProcesses | Sort-Object ServiceName | Format-table Pid, ServiceName, ServiceDisplayName,  ServiceState, Name, Description, ExecutableName, Architecture, ClrVersion

 #$ChoosenId = Read-Host "Choose ID of process to started being debugged in WinDbg"

 #Write-Host "choosen: $ChoosenId"

}

function GetProcessesesByName([string]$name, $allProcesses) {
    return $allProcesses `
     | Where-Object { ($_.name -match ".*$name.*" ) `
        -or ($_.description -match ".*$name.*") `
        -or ((GetFileName -f $_.path) -match ".*$name.*") } `
     | Select-Object `
        @{Expression={GetFileName($_.Id)}; Label="Pid"},
        @{Expression={GetFileName($_.name)}; Label="Name"},
        @{Expression={GetFileName($_.description)}; Label="Description"},
        @{Expression={GetFileName($_.path)}; Label="ExecutableName"},
        @{Expression={GetProcessArchitecture $allProcesses $_.id}; Label="Architecture"},
        @{Expression={GetClrVersion $allProcesses $_.id}; Label="ClrVersion"}
}

function GetProcessesById(
    [string]$procId) {

    return Get-Process `
     | Where-Object { $_.id -eq $procId } `
     | Select-Object `
        @{Expression={GetFileName($_.Id)}; Label="Pid"},
        @{Expression={GetFileName($_.name)}; Label="Name"},
        @{Expression={GetFileName($_.description)}; Label="Description"},
        @{Expression={GetFileName($_.path)}; Label="ExecutableName"},
        @{Expression={GetProcessArchitecture $allProcesses $_.id}; Label="Architecture"},
        @{Expression={GetClrVersion $allProcesses $_.id}; Label="ClrVersion"}
}

function GetIISWorkerProcessesByName([string]$name, $allProcesses) {
    $iisWorkers = Get-ChildItem IIS:\AppPools `
        | Get-ChildItem `
        | Get-ChildItem `
        | Select-Object ProcessId, AppPoolName, State
    
    $processes = @{}

    foreach ($worker in $iisWorkers)
	{
        $processInfo = ($allProcesses | Where-Object Id -eq $worker.ProcessId | Select-Object -First 1)

        if ($worker.AppPoolName -match ".*$name.*" `
            -or $processInfo.Name -match ".*$name.*" `
            -or $processInfo.Description -match ".*$name.*" `
            -or $processInfo.ExecutableName -match ".*$name.*") {

            $processes.Add("Pid", $worker.ProcessId)
            $processes.Add("AppPoolName", $worker.AppPoolName)
            $processes.Add("AppPoolState", $worker.State)
            $processes.Add("Name", $processInfo.Name)
            $processes.Add("Description", $processInfo.Description)
            $processes.Add("ExecutableName", (GetFileName($processInfo.Path)))
            $processes.Add("Architecture", (GetProcessArchitecture $allProcesses $worker.ProcessId))
            $processes.Add("ClrVersion", (GetClrVersion $allProcesses $service.ProcessId))
        }
    }

    return New-Object PSObject -Property $processes;  
}

function GetWindowsServicesByName([string]$name, $allProcesses) {
    $services = Get-CimInstance -ClassName Win32_Service | Select-Object  ProcessId, Name, DisplayName, State
    $processes = @()

    foreach ($service in $services)
	{
        if ($service.ProcessId) {
           $processInfo = ($allProcesses | Where-Object Id -eq $service.ProcessId | Select-Object -First 1)

            if ($service.Name -match ".*$name.*" `
                -or $service.DisplayName -match ".*$name.*" `
                -or $processInfo.Name -match ".*$name.*" `
                -or $processInfo.Description -match ".*$name.*" `
                -or $processInfo.ExecutableName -match ".*$name.*") {

                $process = @{}

                $process.Add("Pid", $service.ProcessId)
                $process.Add("ServiceName", $service.Name)
                $process.Add("ServiceDisplayName", $service.DisplayName)
                $process.Add("ServiceState", $service.State)
                $process.Add("Name", $processInfo.Name)
                $process.Add("Description", $processInfo.Description)
                $process.Add("ExecutableName", (GetFileName($processInfo.Path)))
                $process.Add("Architecture", (GetProcessArchitecture $allProcesses $service.ProcessId))
                $process.Add("ClrVersion", (GetClrVersion $allProcesses $service.ProcessId))

                $processes += New-Object PSObject -Property $process
            }
        }
    }

    return $processes;
}

function GetFileName([string]$fullPath) {
    return [System.IO.Path]::GetFileName($fullPath);
}

function GetProcessArchitecture($allProcesses, $id) {
    $process = ($allProcesses | Where-Object Id -eq $id| Select-Object -First 1)
    $modules = $process.modules
    foreach($module in $modules) {
        $file = [System.IO.Path]::GetFileName($module.FileName).ToLower()
        if($file -eq "wow64.dll") {
            return "x86"
        }
    }
    return "x64"
}

function GetClrVersion($allProcesses, $id) {
    $process = ($allProcesses | Where-Object Id -eq $id| Select-Object -First 1)
    $modules = $process.modules
    foreach($module in $modules) {
        $file = [System.IO.Path]::GetFileName($module.FileName).ToLower()
        if($file -eq "coreclr.dll") {
            return "CORE CLR"
        } elseif ($file -eq "clr.dll") {
            return "CLR 4.0"
        } elseif ($file -eq "mscorwks.dll") {
            return "CLR 2.0"
        } else {
            continue;
        }
    }
    return $null
}

